const User = require("../models/User");
// const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth"); 

module.exports.registerUser = (req,res)=>{

	const hashedPw = bcrypt.hashSync(req.body.password,10);
	//console.log(hashedPw);

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPw,
		mobileNo: req.body.mobileNo

	})

	newUser.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.loginUser = (req,res)=>{
	console.log(req.body);
	User.findOne({email: req.body.email})
	.then(foundUser => {

		// foundUser is the parameter
		if(foundUser === null){
				return res.send({message: "No user found."});
		} else {
			// console.log(foundUser);

			// check the input if it matches
			// compareSync( input string, hashed String)
			const isPasswordCOrrect = bcrypt.compareSync(req.body.password, foundUser.password);
			//console.log(isPasswordCOrrect);
			if(isPasswordCOrrect){
				// create a key to authorized our user
				// we will creat our own module auth.js encoded string that contains our user details. This is what we call a JSONWebToken (JWT)
				return res.send({accessToken: auth.createAccessToken(foundUser)})

				console.log("We will create a token for the user if the password is correct");
			} else{
				return res.send({message: "incorrect Password"})

			}
		}


	})

}