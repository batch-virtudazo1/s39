const Product = require("../models/Product");

module.exports.getAllActiveProducts = (req,res)=>{
	Product.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.addProduct = (req,res)=>{

	let newProduct = new Product({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	})


	//console.log(newProduct);

	newProduct.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

}