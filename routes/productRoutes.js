const express = require("express");
const router = express.Router();

const productControllers = require("../controllers/productControllers");
const auth = require("../auth");
const {verify, verifyAdmin} = auth;

router.get('/', verify, verifyAdmin, productControllers.getAllActiveProducts);

router.post('/addProduct', verify, verifyAdmin, productControllers.addProduct);

module.exports = router;