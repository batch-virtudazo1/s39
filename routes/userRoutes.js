const express = require("express");
const router = express.Router();

// Import controllers
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");
const {verify, verifyAdmin} = auth;

router.post('/', userControllers.registerUser);

router.post('/login', userControllers.loginUser);

module.exports = router;